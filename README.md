# README #

This is a simple fizzBuzz tdd Kata.

## Specifications ##
* Print numbers from 1 - 100
* Print FIZZ for multiples of 3
* Print BUZZ for multiples of 5
* Print FIZZBUZZ for multiples of both 3 and 5

## Test Driven Development (TDD) ##

* Take the first requirement and write test case for that.
* Make the test case pass by writing code for this test.
* Once the test case is passed, refactor

https://docs.google.com/presentation/d/1mndsKFrYbNQT1nZtXpb6oYKwXvBwAJGQCXR7K1hbj0w/edit?usp=sharing

## Setup ##

1. Install node

2. Run `npm install`

3. `npm test` for running test

4. `npm start` for running app