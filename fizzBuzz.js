const FIZZ = "FIZZ";
const BUZZ = "BUZZ";
const FIZZBUZZ = "FIZZBUZZ";

function isMultipleOfThree(number) {
  return number % 3 == 0;
}

function isMultipleOfFive(number) {
  return number % 5 == 0;
}

function fizzBuzz(number) {
  if (isMultipleOfThree(number) && isMultipleOfFive(number)) {
    return FIZZBUZZ;
  }
  if (isMultipleOfThree(number)) {
    return FIZZ;
  }
  if (isMultipleOfFive(number)) {
    return BUZZ;
  }
  return number;

}

module.exports = fizzBuzz;