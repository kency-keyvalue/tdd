const expect = require('chai').expect;
const fizzBuzz = require('../fizzBuzz');

describe('FizzBuzz', function () {
    it('should return number for normal numbers', () => {
        let result = fizzBuzz(1);
        expect(result).to.be.equal(1);
        expect(result).to.be.a('number');
    });

    it('should return FIZZ for number 3', () => {
        let result = fizzBuzz(3);
        expect(result).to.be.equal('FIZZ');
        expect(result).to.be.a('string');
    });

    it('should return FIZZ for number 6', () => {
        let result = fizzBuzz(6);
        expect(result).to.be.equal('FIZZ');
        expect(result).to.be.a('string');
    });

    it('should return BUZZ for number 5', () => {
        let result = fizzBuzz(5);
        expect(result).to.be.equal('BUZZ');
        expect(result).to.be.a('string');
    });

    it('should return BUZZ for number 15', () => {
        let result = fizzBuzz(15);
        expect(result).to.be.equal('FIZZBUZZ');
        expect(result).to.be.a('string');
    });


})